package com.info.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HelloWorldSpringbootApp {
	
	public static void main(String[] args) {
		SpringApplication.run(HelloWorldSpringbootApp.class, args);
    }	}

